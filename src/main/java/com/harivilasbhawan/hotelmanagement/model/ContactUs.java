package com.harivilasbhawan.hotelmanagement.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "Details about contact-us form")
public class ContactUs {
    @ApiModelProperty(name = "name")
    private String name;
    @ApiModelProperty(name = "mobileNo")
    private String mobileNo;
    @ApiModelProperty(name = "message")
    private String message;
}
