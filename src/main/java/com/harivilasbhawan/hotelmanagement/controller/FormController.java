package com.harivilasbhawan.hotelmanagement.controller;

import com.harivilasbhawan.hotelmanagement.model.ContactUs;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contactus")
@Api(tags = "Contact Us")
public class FormController {

    @PostMapping("/submitForm")
    @ApiOperation(value = "Submit contact-us form by guest.", notes = "This API is used to submit form.")
    public String submitForm(@RequestBody ContactUs contactUs) {
        String submitStatus = "Form submitted successfully";

        return submitStatus;
    }

}
